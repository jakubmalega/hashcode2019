﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Hash_Code_2019
{
    public static class PhotoParsers
    {
        public static List<Photo> GetPhotos(string fileName)
        {
            string folder = "input";

            using (StreamReader sr = new StreamReader($"{folder}\\{fileName}"))
            {
                var photos = new List<Photo>();

                sr.ReadLine();

                while (!sr.EndOfStream)
                {
                    var photoLine = sr.ReadLine();

                    var photString = photoLine.Split(' ');

                    Orientation orientation = char.Parse(photString[0]) == 'V'
                        ? Orientation.Vertical
                        : Orientation.Horizontal;

                    string[] tags = new string[photString.Length - 2];

                    Array.Copy(photString, 2, tags, 0, photString.Length - 2);

                    photos.Add(new Photo(orientation, tags));
                }

                return photos;
            }
        }
    }
}