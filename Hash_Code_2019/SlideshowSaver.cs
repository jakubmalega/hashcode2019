﻿using System.Collections.Generic;
using System.IO;

namespace Hash_Code_2019
{
    public class SlideshowSaver
    {
        public SlideshowSaver(string filename, Slideshow slideshow)
        {
            var slides = new List<string>();
            foreach (var slide in slideshow.Slides)
            {
                slides.Add(slide.ToString());
            }
            slides.Insert(0, slides.Count.ToString());
            File.AppendAllLines(Directory.GetCurrentDirectory() + "\\" + "output_" + filename, slides);
        }
    }
}
