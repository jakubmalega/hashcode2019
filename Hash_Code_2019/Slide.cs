﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hash_Code_2019
{
    public class Slide
    {
        public List<Photo> Photos { get; set; }

        public List<string> Tags;

        private List<string> GetTags()
        {
            var tags = new List<string>();

            foreach (var photo in Photos)
            {
                tags.AddRange(photo.Tags.ToList());
            }

            return tags;
        }

        public Slide(List<Photo> photos)
        {
            Photos = photos;

            Tags = GetTags();
        }

        public override string ToString()
        {
            return Photos.Count == 1 ? $"{Photos[0].Id}" : $"{Photos[0].Id} {Photos[1].Id}";
        }
    }
}