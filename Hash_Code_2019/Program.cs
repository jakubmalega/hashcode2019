﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Hash_Code_2019
{
    class Program
    {
        static void Main(string[] args)
        {
            // const string FILENAME = "a_example.txt";
            // const string FILENAME = "b_lovely_landscapes.txt";
            // const string FILENAME = "c_memorable_moments.txt";
            // const string FILENAME = "d_pet_pictures.txt";
            const string FILENAME = "e_shiny_selfies.txt";

            var allPhotos = PhotoParsers.GetPhotos(FILENAME);

            // Find most different Vert
            var verticalPhotos = allPhotos.Where(x => x.Orientation == Orientation.Vertical).ToList();

            List<Slide> verticalSlides = new List<Slide>();

            ConnectVertical(verticalPhotos, verticalSlides);

            var horizontalSlides = allPhotos.Where(x => x.Orientation == Orientation.Horizontal)
                .Select(x=> new Slide(new List<Photo>() { x })).ToList();

            var allSlides = new List<Slide>();
            allSlides.AddRange(verticalSlides);
            allSlides.AddRange(horizontalSlides);

            var slideshow = new Slideshow();

            int minIterestFactor = (allSlides.Max(x => x.Tags.Count) / 2);

            int sumInterest = 0;

            while (allSlides.Count > 0 && minIterestFactor >= 0)
            {
                for (int i = 0; i <= allSlides.Count - 1; i++)
                {
                    Slide slide1;
                    Slide slide2;

                    if (slideshow.Slides.Count == 0)
                    {
                        if (i + 1 >= allSlides.Count)
                        {
                            continue;
                        }

                        slide1 = allSlides[i];

                        slide2 = allSlides[i + 1];

                        int interestFactor = CalcInterestFactor(slide1, slide2);

                        if (interestFactor >= minIterestFactor)
                        {
                            slideshow.Slides.Add(allSlides[i]);
                            slideshow.Slides.Add(allSlides[i + 1]);

                            allSlides.RemoveAt(i);
                            allSlides.RemoveAt(i);

                            sumInterest += interestFactor;

                            i = i - 1;
                        }
                    }
                    else
                    {
                        slide1 = slideshow.Slides.Last();
                        slide2 = allSlides[i];

                        int interestFactor = CalcInterestFactor(slide1, slide2);

                        if (interestFactor >= minIterestFactor)
                        {
                            slideshow.Slides.Add(allSlides[i]);

                            allSlides.RemoveAt(i);
                            sumInterest += interestFactor;
                            i = i - 1;
                        }
                    }
                }

                minIterestFactor--;
            }

            Console.WriteLine(sumInterest);
            Console.ReadKey();
        }

        private static void ConnectVertical(List<Photo> verticalPhotos, List<Slide> verticalSlides)
        {
            int counter = 0;

            while (verticalPhotos.Count > 0)
            {
                for (int i = 0; i < verticalPhotos.Count - 1; i++)
                {
                    int common = verticalPhotos[i].Tags.Intersect(verticalPhotos[i + 1].Tags).Count();

                    if (common <= counter)
                    {
                        var slide = new Slide(new List<Photo>() {verticalPhotos[i], verticalPhotos[i + 1]});

                        verticalSlides.Add(slide);

                        verticalPhotos.RemoveAt(i);
                        verticalPhotos.RemoveAt(i);

                        i = i - 1;
                    }
                }

                counter++;
            }
        }

        private static int CalcInterestFactor(Slide sliede1, Slide sliede2)
        {
            int commonParts = sliede1.Tags.Intersect(sliede2.Tags).Count();

            int slide1Uniq = sliede1.Tags.Count - commonParts;

            int slide2Uniq = sliede1.Tags.Count - commonParts;

            return Math.Min(Math.Min(commonParts, slide1Uniq), slide2Uniq);
        }
    }
}
