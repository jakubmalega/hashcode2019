﻿using System.Collections.Generic;

namespace Hash_Code_2019
{
    public class Photo
    {
        private static int id;

        public int Id { get; }
        public Orientation Orientation { get; set; }

        public string[] Tags { get; set; }

        public Photo(Orientation orientation, string[] tags)
        {
            Id = id++;
            Orientation = orientation;
            Tags = tags;
        }
    }
}