﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hash_Code_2019
{
    public class Slideshow
    {
        public List<Slide> Slides { get; set; }

        public Slideshow()
        {
            Slides = new List<Slide>();
        }
    }
}
